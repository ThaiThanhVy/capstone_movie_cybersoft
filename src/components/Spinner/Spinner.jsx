import React from 'react'
import { useSelector } from 'react-redux'
import { HashLoader } from 'react-spinners'

export default function Spinner() {
    let { isLoading } = useSelector(state => state.reducerSpinner)

    return isLoading ? (
        <div className='h-screen w-screen fixed top-0 left-0 bg-fuchsia-500 z-50 flex justify-center items-center'>
            <HashLoader size={50} color="#182747" />
        </div>
    ) : (<></>)
}
